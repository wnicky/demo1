import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent }   from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { DropdownModule } from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';
import {MultiSelectModule} from 'primeng/multiselect';
import {CheckboxModule} from 'primeng/checkbox';
import {ConfirmDialogModule} from 'primeng/confirmdialog';

import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SellerformComponent } from './sellerform/sellerform.component';
import { SellerListComponent } from './seller-list/seller-list.component';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DropdownModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ButtonModule,
    InputTextModule,
    CalendarModule,
    MultiSelectModule,
    ConfirmDialogModule,
    CommonModule,
    TranslateModule,
    CheckboxModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' })

    
  ],
  declarations: [ AppComponent, SellerformComponent, SellerListComponent ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
