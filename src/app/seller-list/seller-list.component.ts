import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ConfirmationService } from 'primeng/api';
import { Message } from 'primeng/api';

@Component({
  selector: 'app-seller-list',
  templateUrl: './seller-list.component.html',
  styleUrls: ['./seller-list.component.css'],
  providers: [ConfirmationService]
})

export class SellerListComponent implements OnInit {
  dataList: any[];
  saveForEdits: any;
  msgs: Message[] = [];
  isEdit = false;
  editIndexPass: any;
  constructor(private dataService: DataService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.isEdit
    this.dataService.pushData.subscribe((data: any) => {
      this.dataList = data;
      this.saveForEdits = data;
    });

  }
  deleteFun(i) {
    let deletedFilter: any[] = this.dataList;
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.dataList.splice(i, 1);

        this.msgs = [{ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' }];
      },
      reject: () => {
        this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  editFun(i) {
    let editedData = [];
    this.editIndexPass = i;
    this.dataService.editClicked.next(i);
    this.dataService.isEdit.next(true);
    editedData.push(this.saveForEdits[i]);
    this.dataService.edit(editedData);
  }
}
