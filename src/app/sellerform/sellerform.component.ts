import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../services/data.service';

interface Offices {
  name: string,
  code: string
}
@Component({
  selector: 'app-sellerform',
  templateUrl: './sellerform.component.html',
  styleUrls: ['./sellerform.component.css']
})

export class SellerformComponent implements OnInit {
  sellerForm = new FormGroup({
    sellerName: new FormControl('',Validators.required),
    currency: new FormControl('',Validators.required),
    offices: new FormControl('',Validators.required),
      bidded:new FormControl(''),
      guaranteed:new FormControl(''),
    contactName: new FormControl(''),
      email: new FormControl('',Validators.pattern("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$")),
  });

  currencyArray:any[]
  selectedCurrency:Offices[];
  officeArray: Offices[];
  selectedOffice:Offices[];
  allFormsData= [];
  editList: any;
  editChecked:boolean= false;
  updateIndex:any;

  showErrorMessage: boolean = false;
  biddedvalue:boolean =true;
  guaranteedvalue:boolean= false ;
  formList=[];

  constructor(private dataService:DataService) {
    this.officeArray = [
      {name: 'New York', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
    ];

    this.currencyArray = [
        {name: 'USD'},
        {name: 'EUR'},
        {name: 'GBP'},
        {name: 'RUB'},
        {name: 'Paris'}];
  }

  ngOnInit() {
      this.checkEdit()
    
  }

checkEdit(){
  this.dataService.editClicked.subscribe(data => {
    this.updateIndex = data; 
    if(this.updateIndex!==''|| this.updateIndex!==null){
      this.editChecked = true;
      this.getEditList();
    }
});
}
getEditList(){
  this.dataService.editData.subscribe(data => {
    this.editList = data;
    this.sellerForm.get('sellerName').setValue(this.editList[0].sellerName);
    this.sellerForm.get('currency').setValue(this.editList[0].currency);
    this.sellerForm.get('offices').setValue(this.editList[0].offices);
    this.sellerForm.get('bidded').setValue(this.editList[0].bidded);
    this.sellerForm.get('guaranteed').setValue(this.editList[0].guaranteed);
    this.sellerForm.get('contactName').setValue(this.editList[0].contactName);
    this.sellerForm.get('email').setValue(this.editList[0].email);

    console.log('this.editList.sellerName', this.editList[0]);
 });
}

  onSubmit() {
    if (this.sellerForm.valid && !this.sellerForm.errors){
      if(this.editChecked){
        let abc  =Array(this.sellerForm.value);
        this.allFormsData.splice(this.updateIndex, 1);
        this.allFormsData.push(this.sellerForm.value)
      }else{
        this.allFormsData.push(this.sellerForm.value);

      }
      console.log('allFormsData',this.allFormsData);
      this.dataService.push(this.allFormsData); 
      this.editChecked =false;
      this.showErrorMessage = false;

      this.sellerForm.reset();
      
    }else{
      this.showErrorMessage = true;
    }
  }

  cancelForm(){
    this.showErrorMessage = false;
    this.editChecked =false;

    this.sellerForm.reset();
  }
}
