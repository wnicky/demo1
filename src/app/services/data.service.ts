import { EventEmitter, Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  pushData = new EventEmitter<string>();
  editData = new EventEmitter<string>();
  updateData = new EventEmitter<string>();

  public editClicked = new Subject<any[]>();
  public isEdit = new Subject<any>();

  push(value){
      this.pushData.emit(value);
  }
  edit(value){
     this.editData.emit(value);
  }

  constructor() { }

}
